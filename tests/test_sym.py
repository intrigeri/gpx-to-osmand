"""Test GPX <sym> handling"""

import logging

import pytest

import gpx_to_osmand

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


def test_missing_mapping(shared_datadir):
    """Icon found in input file but not in configuration raises an exception"""
    with pytest.raises(
        gpx_to_osmand.MissingMappings, match="Missing mappings"
    ):
        gpx_to_osmand.Processor(
            input_file=shared_datadir / "test.gpx",
            config_file=shared_datadir / "missing_mapping.yml",
        )


def test_read_input_symbols(shared_datadir):
    """Can extract list of <sym> in input file"""
    processor = gpx_to_osmand.Processor(
        input_file=shared_datadir / "test.gpx",
        config_file=shared_datadir / "valid.yml",
    )
    assert len(processor.input_symbols()) == 2


def test_osmand_icon(shared_datadir):
    """Maps <sym> to OsmAnd icon"""
    processor = gpx_to_osmand.Processor(
        input_file=shared_datadir / "test.gpx",
        config_file=shared_datadir / "valid.yml",
    )
    processor.process()
    for waypoint in processor.gpx.waypoints:
        if waypoint.name.startswith("Camping"):
            assert (
                processor.osmand_icon(waypoint)["icon"] == "tourism_camp_site"
            )
        elif waypoint.name.startswith("Hotel"):
            assert processor.osmand_icon(waypoint)["icon"] == "tourism_hotel"


def test_add_osmand_elements(shared_datadir):
    """Adds osmand:* elements to GPX"""
    processor = gpx_to_osmand.Processor(
        input_file=shared_datadir / "test.gpx",
        config_file=shared_datadir / "valid.yml",
    )
    processor.process()
    for waypoint in processor.gpx.waypoints:
        osmand_icon_element = [
            e for e in waypoint.extensions if e.tag == "osmand:icon"
        ][0]
        osmand_icon = osmand_icon_element.text
        if waypoint.name.startswith("Camping"):
            assert osmand_icon == "tourism_camp_site"
        elif waypoint.name.startswith("Hotel"):
            assert osmand_icon == "tourism_hotel"
