"""Test GPX history cleanup functionality"""

import logging

import gpxpy.gpx

import gpx_to_osmand

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


def assert_waypoint_has_no_ql_history(waypoint: gpxpy.gpx.GPXWaypoint) -> None:
    """Raises exception if `waypoint` has a ql:history extension"""
    assert (
        len(
            [
                e
                for e in waypoint.extensions
                if e.tag
                == "{http://www.qlandkarte.org/xmlschemas/v1.1}history"
            ]
        )
        == 0
    )


def test_simplify_waypoints(shared_datadir):
    """Remove unwanted data from GPX waypoints"""
    processor = gpx_to_osmand.Processor(
        input_file=shared_datadir / "test.gpx",
        config_file=shared_datadir / "valid.yml",
    )
    processor.process()
    for waypoint in processor.gpx.waypoints:
        assert waypoint.time is None
        assert_waypoint_has_no_ql_history(waypoint)
