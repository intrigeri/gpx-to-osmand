"""Convert GPX to OsmAnd format and simplify it."""

import xml.etree.ElementTree as ET  # nosec
from logging import getLogger
from pathlib import Path
from typing import Optional, Set

import gpxpy
import gpxpy.gpx
import yaml

log = getLogger(__name__)


class MissingMappings(Exception):
    """Configuration file lacks mapping for some <sym> in the input file."""

    def __init__(self, missing_mappings):
        self.missing_mappings = missing_mappings

    def config_sample(self):
        """Return YAML config sample to fix the problem."""
        config = {
            "icons_map": {
                i: {"icon": "FIXME", "color": "FIXME", "background": "FIXME"}
                for i in self.missing_mappings
            }
        }
        return yaml.safe_dump(config)

    def __str__(self):
        """Suggest configuration to fix the problem."""
        return (
            f"Missing mappings: {self.missing_mappings}\n"
            "To fix this, add to your configuration file:\n\n"
            + self.config_sample()
            + "\n\n"
            "Icons supported by OsmAnd: "
            "https://github.com/osmandapp/OsmAnd-resources/"
            "blob/master/poi/poi_categories.json"
        )


class Processor:
    """Convert GPX to OsmAnd format and simplify it."""

    def __init__(self, input_file: Path, config_file: Path):
        with open(config_file, "r", encoding="utf-8") as conf_fd:
            self.config = yaml.safe_load(conf_fd)
        if not isinstance(self.config, dict):
            self.config = {}
            self.config["icons_map"] = {}
        with open(input_file, "r", encoding="utf-8") as gpx_fd:
            self.gpx = gpxpy.parse(gpx_fd)
        self.validate_config()

    def validate_config(self) -> None:
        """Ensure configuration is valid, else raise."""
        missing_mappings = self.input_symbols() - set(
            self.config["icons_map"].keys()
        )
        if missing_mappings:
            raise MissingMappings(missing_mappings)

    def input_symbols(self) -> Set[Optional[str]]:
        """List unique symbols used in the input file."""
        return {waypoint.symbol for waypoint in self.gpx.waypoints}

    def osmand_icon(self, waypoint: gpxpy.gpx.GPXWaypoint) -> dict:
        """Name of the OsmAnd icon to use for `waypoint`."""
        icon = self.config["icons_map"][waypoint.symbol]
        return {
            "icon": icon["icon"],
            "color": icon["color"],
            "background": icon["background"],
        }

    def add_osmand_elements(self) -> None:
        """Add osmand:* elements to waypoints."""
        for waypoint in self.gpx.waypoints:
            if (
                len(
                    [e for e in waypoint.extensions if e.tag == "osmand::icon"]
                )
                > 0
            ):
                log.warning(
                    "Waypoint '%s' already has a OsmAnd icon skipping.",
                    waypoint.name,
                )
            icon = self.osmand_icon(waypoint)
            for k in ["icon", "color", "background"]:
                osmand_element = ET.Element(f"osmand:{k}")
                osmand_element.text = icon[k]
                waypoint.extensions.append(osmand_element)

    def simplify_waypoints(self) -> None:
        """Remove unwanted elements from waypoints."""
        for waypoint in self.gpx.waypoints:
            waypoint.time = None
            history_tag = "{http://www.qlandkarte.org/xmlschemas/v1.1}history"
            waypoint.extensions = [
                e for e in waypoint.extensions if e.tag != history_tag
            ]

    def process(self) -> None:
        """Process self.gpx."""
        self.add_osmand_elements()
        self.simplify_waypoints()

    def to_xml(self) -> str:
        """Return GPX as a XML 1.1 string."""
        self.gpx.nsmap["osmand"] = "https://osmand.net"
        return self.gpx.to_xml(version="1.1")
