[![pipeline status](https://salsa.debian.org/intrigeri/gpx-to-osmand/badges/main/pipeline.svg)](https://salsa.debian.org/intrigeri/gpx-to-osmand/-/commits/main)

[[_TOC_]]

# What this does

Converts GPX, generated for example with
[QMapShack](https://github.com/Maproom/qmapshack), into a format that works
better in [OsmAnd](https://osmand.net/):

 - Converts waypoint icons from GPX standard (`<sym>`) to the format supported
   by OsmAnd (`osmand:icon`, `osmand:background`, and `osmand:color`
   extensions), with user-provided mapping.

   Rationale: OsmAnd [does not support
   `sym`](https://github.com/osmandapp/OsmAnd/issues/11285).

 - Simplify GPX:

   - Remove `<time>` element.
   - Remove `<ql:history>` extensions added by QMapShack.

# Usage

See `./gpx-to-osmand --help`.

You will need a configuration file; for inspiration, see the [example
configuration file](tests/data/valid.yml).

# Dependencies

```shell
sudo apt install \
   python3-coloredlogs
   python3-gpxpy \
   python3-xdg
   python3-yaml
```

# License

Copyright 2022 © intrigeri <intrigeri@boum.org>

GPLv3+
